package torshin.virtu;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.offset;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import torshin.virtu.model.RealtyType;
import torshin.virtu.model.dto.PremiumDto;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ContractControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void contractsList() throws Exception {
    this.mockMvc.perform(get("/contracts")).andDo(print()).andExpect(status().isOk())
        .andExpect(content().contentType("application/hal+json;charset=UTF-8"));
  }

  @Test
  public void premiumCalcTest() throws Exception {

    long insuredSum = 1000L;
    double area = 51.;
    int duration = 30;
    int year = 2005;
    PremiumDto premiumDto = new PremiumDto();
    premiumDto.setArea(area);
    premiumDto.setDurationStart(LocalDate.now());
    premiumDto.setDurationEnd(LocalDate.now().plusDays(duration));
    premiumDto.setInsuredSum(insuredSum);
    premiumDto.setYear(year);
    premiumDto.setRealtyType(RealtyType.ROOM);

    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
    mapper.registerModule(new JavaTimeModule());
    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    ObjectWriter ow = mapper.writer();
    String requestJson = ow.writeValueAsString(premiumDto);

    premiumDto.setCalculationDate(LocalDate.now());
    premiumDto.setPremium(insuredSum * 1.0 / duration * 1.3 * 1.6 * 1.5);
    String responseJson = ow.writeValueAsString(premiumDto);
    String url = "/contract/premium-calc";
    MvcResult result = this.mockMvc
        .perform(post(url).content(requestJson).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andReturn();

    PremiumDto responsePremiumDto = mapper
        .readValue(result.getResponse().getContentAsString(), PremiumDto.class);
    assertThat(premiumDto.getPremium()).isCloseTo(responsePremiumDto.getPremium(), offset(0.001));
  }
}
