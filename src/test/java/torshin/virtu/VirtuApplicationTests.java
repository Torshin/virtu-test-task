package torshin.virtu;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.offset;

import java.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import torshin.virtu.model.RealtyType;
import torshin.virtu.model.dto.PremiumDto;
import torshin.virtu.service.ContractsService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class VirtuApplicationTests {

  @Autowired
  private ContractsService contractsService;

  @Test
  public void premiumCalcUnitTest() {
    long insuredSum = 1000L;
    double area = 51.;
    int duration = 30;
    int year = 2005;
    PremiumDto premiumDto = new PremiumDto();
    premiumDto.setArea(area);
    premiumDto.setDurationStart(LocalDate.now());
    premiumDto.setDurationEnd(LocalDate.now().plusDays(duration));
    premiumDto.setInsuredSum(insuredSum);
    premiumDto.setYear(year);
    premiumDto.setRealtyType(RealtyType.ROOM);
    double premium = insuredSum * 1.0 / duration * 1.3 * 1.6 * 1.5;
    assertThat(contractsService.calcPremium(premiumDto)).isCloseTo(premium, offset(0.001));
  }

}
