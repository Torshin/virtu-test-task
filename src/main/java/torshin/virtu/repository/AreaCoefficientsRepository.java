package torshin.virtu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import torshin.virtu.model.coefficient.AreaCoefficient;

@Repository
public interface AreaCoefficientsRepository extends JpaRepository<AreaCoefficient, Long> {

  @Query("select c from AreaCoefficient c where (?1 >= c.areaFrom or c.areaFrom is null) "
      + "and (?1 <= c.areaTo or c.areaTo is null )")
  public AreaCoefficient getAreaCoefficientByArea(double area);
}
