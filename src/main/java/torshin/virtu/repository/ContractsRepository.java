package torshin.virtu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import torshin.virtu.model.Contract;

@Repository
public interface ContractsRepository extends JpaRepository<Contract, Long> {

}
