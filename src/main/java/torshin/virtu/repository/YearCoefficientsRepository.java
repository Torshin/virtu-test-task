package torshin.virtu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import torshin.virtu.model.coefficient.YearCoefficient;

@Repository
public interface YearCoefficientsRepository extends JpaRepository<YearCoefficient, Long> {

  @Query("select c from YearCoefficient c where (?1 >= c.yearFrom or c.yearFrom is null) "
      + "and (?1 <= c.yearTo or c.yearTo is null)")
  public YearCoefficient getYearCoefficientByYear(int year);
}
