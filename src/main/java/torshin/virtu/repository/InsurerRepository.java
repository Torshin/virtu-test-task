package torshin.virtu.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import torshin.virtu.model.Insurer;

public interface InsurerRepository extends CrudRepository<Insurer, Long> {

  @Query("select i from Insurer i where (?1 is null or UPPER(i.firstName) like UPPER('%' || ?1 ||'%')) "
      + "and (?2 is null or UPPER(i.lastName) like UPPER('%'|| ?2 ||'%')) "
      + "and ((?3 is null or ?3 = '') or UPPER(i.middleName) like UPPER('%'|| ?3 ||'%'))")
  public List<Insurer> findAllByName(@Param("firstName") String firstName,
      @Param("lastName") String lastName, @Param("middleName") String middleName);
}
