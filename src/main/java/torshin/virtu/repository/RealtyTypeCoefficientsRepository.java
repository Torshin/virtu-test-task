package torshin.virtu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import torshin.virtu.model.RealtyType;
import torshin.virtu.model.coefficient.RealtyTypeCoefficient;

@Repository
public interface RealtyTypeCoefficientsRepository extends
    JpaRepository<RealtyTypeCoefficient, Long> {

  public RealtyTypeCoefficient getRealtyTypeCoefficientByRealtyType(RealtyType area);
}
