package torshin.virtu.controller;

import java.time.LocalDate;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import torshin.virtu.model.Contract;
import torshin.virtu.model.Insurer;
import torshin.virtu.model.dto.PremiumDto;
import torshin.virtu.repository.ContractsRepository;
import torshin.virtu.repository.InsurerRepository;
import torshin.virtu.service.ContractsService;

@RestController
@RequestMapping("contract")
public class ContractController {

  private final ContractsRepository contractsRepository;
  private final InsurerRepository insurerRepository;
  private final ContractsService contractsService;

  public ContractController(
      ContractsRepository contractsRepository,
      InsurerRepository insurerRepository,
      ContractsService contractsService) {
    this.contractsRepository = contractsRepository;
    this.insurerRepository = insurerRepository;
    this.contractsService = contractsService;
  }

  @PostMapping(value = "premium-calc")
  @ResponseBody
  public PremiumDto calcPremium(@Valid @RequestBody PremiumDto premiumDTO) {
    double premium = contractsService.calcPremium(premiumDTO);
    premiumDTO.setCalculationDate(LocalDate.now());
    premiumDTO.setPremium(premium);
    return premiumDTO;
  }

  @RequestMapping(method = RequestMethod.POST)
  @ResponseBody
  public Contract save(@Valid @RequestBody Contract contract) {
    Insurer insurer = insurerRepository.findById(contract.getInsurer().getId())
        .orElseThrow(() -> new IllegalStateException("Страхователь не найден"));
    insurer.copy(contract.getInsurer());
    contract.setInsurer(insurer);
    contract.setDate(LocalDate.now());
    contract.setPremium(contractsService.calcPremium(new PremiumDto(contract)));
    return contractsRepository.save(contract);
  }


}
