package torshin.virtu.controller;

import java.util.Arrays;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import torshin.virtu.model.RealtyType;

@RestController
@RequestMapping("realtyTypes")
public class RealtyTypeController {

  @GetMapping
  public List<RealtyType> realtyTypes() {
    return Arrays.asList(RealtyType.values());
  }
}
