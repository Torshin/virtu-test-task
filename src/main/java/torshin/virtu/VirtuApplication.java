package torshin.virtu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VirtuApplication  {

    public static void main(String[] args) {
        SpringApplication.run(VirtuApplication.class, args);
    }

}
