package torshin.virtu.service;

import java.time.temporal.ChronoUnit;
import org.springframework.stereotype.Service;
import torshin.virtu.model.dto.PremiumDto;
import torshin.virtu.repository.AreaCoefficientsRepository;
import torshin.virtu.repository.RealtyTypeCoefficientsRepository;
import torshin.virtu.repository.YearCoefficientsRepository;

@Service
public class ContractsService {

  private AreaCoefficientsRepository areaCoefficientsRepository;
  private YearCoefficientsRepository yearCoefficientsRepository;
  private RealtyTypeCoefficientsRepository realtyTypeCoefficientsRepository;

  public ContractsService(AreaCoefficientsRepository areaCoefficientsRepository,
      YearCoefficientsRepository yearCoefficientsRepository,
      RealtyTypeCoefficientsRepository realtyTypeCoefficientsRepository) {
    this.areaCoefficientsRepository = areaCoefficientsRepository;
    this.yearCoefficientsRepository = yearCoefficientsRepository;
    this.realtyTypeCoefficientsRepository = realtyTypeCoefficientsRepository;
  }

  public double calcPremium(PremiumDto premiumDto) {
    if (premiumDto == null) {
      throw new IllegalArgumentException("Contract can't be null");
    }
    long sum = premiumDto.getInsuredSum();
    long days = ChronoUnit.DAYS.between(premiumDto.getDurationStart(), premiumDto.getDurationEnd());
    int year = premiumDto.getYear();

    double yearCoefficient = yearCoefficientsRepository
        .getYearCoefficientByYear(year).getCoefficient();
    double areaCoefficient = areaCoefficientsRepository
        .getAreaCoefficientByArea(premiumDto.getArea()).getCoefficient();
    double realtyTypeCoefficient = realtyTypeCoefficientsRepository
        .getRealtyTypeCoefficientByRealtyType(premiumDto.getRealtyType()).getCoefficient();
    return (((double) (sum)) / days) * areaCoefficient * yearCoefficient * realtyTypeCoefficient;
  }
}
