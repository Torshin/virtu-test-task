package torshin.virtu.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import lombok.Data;

@Data
@Entity
public class Insurer {

  @Id
  @GeneratedValue
  private Long id;
  @NotNull(message = "Заполните фамилию")
  private String lastName;
  @NotNull(message = "Заполните имя")
  private String firstName;
  private String middleName;
  @JsonFormat(pattern = "dd.MM.yyyy")
  @NotNull(message = "Заполните дату рождения")
  @PastOrPresent
  private Date birthDate;
  @Min(value = 1000, message = "Серия должна содержать 4 цифр")
  @Max(value = 9999, message = "Серия должна содержать 4 цифр")
  private Integer passportSerial;
  @Min(value = 100_000, message = "Номер должен содержать 6 цифр")
  @Max(value = 999_999, message = "Номер должен содержать 6 цифр")
  private Integer passportNumber;


  public void copy(Insurer insurer) {
    lastName = insurer.getLastName();
    firstName = insurer.getFirstName();
    middleName = insurer.getMiddleName();
    birthDate = insurer.getBirthDate();
    passportSerial = insurer.getPassportSerial();
    passportNumber = insurer.getPassportNumber();
  }
}
