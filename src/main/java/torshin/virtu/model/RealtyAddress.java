package torshin.virtu.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.Data;

@Entity
@Data
public class RealtyAddress {
  @Id
  @GeneratedValue
  private Long id;
  @NotEmpty(message = "Заполните страну")
  private String country;
  private String index;
  @NotEmpty(message = "Заполните регион")
  private String region;
  private String district;
  @NotEmpty(message = "Заполните город")
  private String city;
  @NotEmpty(message = "Заполните улицу")
  private String street;
  private Integer house;
  private String building;
  private String structure;
  @NotNull(message = "Заполните квартиру")
  @Positive
  private Integer apartment;

}
