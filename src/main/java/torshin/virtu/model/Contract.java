package torshin.virtu.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;
import lombok.Data;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Data
public class Contract {

  private static final String DD_MM_YYYY = "dd.MM.yyyy";
  @Id
  @GeneratedValue
  private Long id;
  @NotNull(message = "Добавьте страховую сумму")
  private Long insuredSum;
  @JsonFormat(pattern = DD_MM_YYYY)
  @NotNull(message = "Добавьте дату начала")
  @FutureOrPresent(message = "Дата начала должна быть не меньше текущей")
  private LocalDate durationStart;
  @JsonFormat(pattern = DD_MM_YYYY)
  @NotNull(message = "Добавьте дату конца")
  private LocalDate durationEnd;
  @NotNull(message = "Выберите тип недвижимости")
  private RealtyType realtyType;
  @NotNull(message = "Добавьте год постройки")
  @Min(value = 1000, message = "Год должен быть четерёхзначным")
  private Integer year;
  @NotNull(message = "Добавьте площадь")
  @Positive
  private double area;
  @JsonFormat(pattern = DD_MM_YYYY)
  private LocalDate calculationDate;
  private Double premium;

  @AssertTrue(message = "Срок действия договора должен быть не больше одного года")
  private boolean isOkDuration() {
    if (durationStart != null && durationEnd != null) {
      return ChronoUnit.YEARS.between(durationStart, durationEnd.minusDays(1)) < 1;
    }
    return false;
  }

  @AssertTrue(message = "\"Срок действия по\" должен быть больше \"Срок действия с\"")
  private boolean isPositiveDuration() {
    if (durationStart != null && durationEnd != null) {
      return durationStart.isBefore(durationEnd);
    }
    return false;
  }

  @AssertTrue(message = "Год постройки не может быть больше текущего")
  private boolean isOkYear() {
    if (year != null) {
      return year <= LocalDate.now().getYear();
    }
    return false;
  }


  @NotNull(message = "Добавьте номер договора")
  @Min(value = 100_000, message = "Договор должен содержать 6 цифр")
  @Max(value = 999_999, message = "Договор должен содержать 6 цифр")
  private Integer contractNumber;
  @DateTimeFormat(pattern = DD_MM_YYYY)
  @JsonFormat(pattern = DD_MM_YYYY)
  @PastOrPresent
  private LocalDate date;
  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn
  @RestResource(exported = false)
  @NotNull
  @Valid
  private Insurer insurer;
  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn
  @NotNull
  @Valid
  private RealtyAddress realtyAddress;
  private String comment;
}

