package torshin.virtu.model.coefficient;

import javax.persistence.Entity;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class AreaCoefficient extends Coefficient{


  public AreaCoefficient(Double areaFrom, Double areaTo, double coefficient) {
    this.areaFrom = areaFrom;
    this.areaTo = areaTo;
    this.coefficient = coefficient;
  }

  private Double areaFrom;

  private Double areaTo;

}
