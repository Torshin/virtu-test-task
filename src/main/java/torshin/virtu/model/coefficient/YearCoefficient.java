package torshin.virtu.model.coefficient;

import javax.persistence.Entity;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class YearCoefficient extends Coefficient {

  private Integer yearFrom;

  private Integer yearTo;

  public YearCoefficient(Integer yearFrom, Integer yearTo, double coefficient) {
    this.yearFrom = yearFrom;
    this.yearTo = yearTo;
    this.coefficient = coefficient;
  }

}
