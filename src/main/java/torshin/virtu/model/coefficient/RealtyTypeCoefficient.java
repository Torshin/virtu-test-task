package torshin.virtu.model.coefficient;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import lombok.Data;
import lombok.NoArgsConstructor;
import torshin.virtu.model.RealtyType;

@Entity
@Data
@NoArgsConstructor
public class RealtyTypeCoefficient extends Coefficient {

  @Enumerated(EnumType.STRING)
  private RealtyType realtyType;

  public RealtyTypeCoefficient(RealtyType realtyType, double coefficient) {
    this.realtyType = realtyType;
    this.coefficient = coefficient;
  }

}
