package torshin.virtu.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.Data;
import lombok.NoArgsConstructor;
import torshin.virtu.model.Contract;
import torshin.virtu.model.RealtyType;

@Data
@NoArgsConstructor
public class PremiumDto {

  private static final String DD_MM_YYYY = "dd.MM.yyyy";

  @NotNull(message = "Добавьте страховую сумму")
  private Long insuredSum;
  @JsonFormat(pattern = DD_MM_YYYY)
  @NotNull(message = "Добавьте дату начала")
  @FutureOrPresent(message = "Дата начала должна быть не меньше текущей")
  private LocalDate durationStart;
  @JsonFormat(pattern = DD_MM_YYYY)
  @NotNull(message = "Добавьте дату конца")
  private LocalDate durationEnd;
  @NotNull(message = "Выберите тип недвижимости")
  private RealtyType realtyType;
  @NotNull(message = "Добавьте год постройки")
  @Min(value = 1000, message = "Год должен быть четерёхзначным")
  private Integer year;
  @NotNull(message = "Добавьте площадь")
  @Positive
  private double area;
  @JsonFormat(pattern = DD_MM_YYYY)
  private LocalDate calculationDate;
  private Double premium;

  public PremiumDto(Contract contract) {
    insuredSum = contract.getInsuredSum();
    durationStart = contract.getDurationStart();
    durationEnd = contract.getDurationEnd();
    realtyType = contract.getRealtyType();
    year = contract.getYear();
    area = contract.getArea();
    calculationDate = contract.getCalculationDate();
    premium = contract.getPremium();
  }

  @AssertTrue(message = "Срок действия договора должен быть не больше одного года")
  private boolean isOkDuration() {
    if (durationStart != null && durationEnd != null) {
      return ChronoUnit.YEARS.between(durationStart, durationEnd.minusDays(1)) < 1;
    }
    return false;
  }

  @AssertTrue(message = "Срок действия договора должен быть не больше одного года")
  private boolean isPositiveDuration() {
    if (durationStart != null && durationEnd != null) {
      return durationStart.isBefore(durationEnd);
    }
    return false;
  }

  @AssertTrue(message = "Год постройки не может быть больше текущего")
  private boolean isOkYear() {
    if (year != null) {
      return year <= LocalDate.now().getYear();
    }
    return false;
  }

}
