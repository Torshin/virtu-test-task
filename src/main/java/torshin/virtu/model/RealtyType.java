package torshin.virtu.model;

public enum RealtyType {
  APARTMENT,
  HOUSE,
  ROOM
}
