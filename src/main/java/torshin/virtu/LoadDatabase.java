package torshin.virtu;

import java.util.ArrayList;
import java.util.List;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import torshin.virtu.model.RealtyType;
import torshin.virtu.model.coefficient.AreaCoefficient;
import torshin.virtu.model.coefficient.RealtyTypeCoefficient;
import torshin.virtu.model.coefficient.YearCoefficient;
import torshin.virtu.repository.AreaCoefficientsRepository;
import torshin.virtu.repository.RealtyTypeCoefficientsRepository;
import torshin.virtu.repository.YearCoefficientsRepository;

@Configuration
public class LoadDatabase {

  @Bean
  CommandLineRunner initAreaCoefficients(AreaCoefficientsRepository repository) {
    return args -> {
      List<AreaCoefficient> areaCoefficients = new ArrayList<>();
      areaCoefficients.add(new AreaCoefficient(null, 49.99, 1.2));
      areaCoefficients.add(new AreaCoefficient(50.0, 100.0, 1.5));
      areaCoefficients.add(new AreaCoefficient(100.01, null, 2));
      List<AreaCoefficient> all = repository.findAll();
      areaCoefficients.removeAll(all);
      repository.saveAll(areaCoefficients);
    };
  }

  @Bean
  CommandLineRunner initYearCoefficients(YearCoefficientsRepository repository) {
    return args -> {
      List<YearCoefficient> yearCoefficients = new ArrayList<>();
      yearCoefficients.add(new YearCoefficient(null, 1999, 1.3));
      yearCoefficients.add(new YearCoefficient(2000, 2014, 1.6));
      yearCoefficients.add(new YearCoefficient(2015, null, 2));
      List<YearCoefficient> all = repository.findAll();
      yearCoefficients.removeAll(all);
      repository.saveAll(yearCoefficients);
    };
  }

  @Bean
  CommandLineRunner initRealtyTypeCoefficients(RealtyTypeCoefficientsRepository repository) {
    return args -> {
      List<RealtyTypeCoefficient> realtyTypeCoefficients = new ArrayList<>();
      realtyTypeCoefficients.add(new RealtyTypeCoefficient(RealtyType.APARTMENT, 1.7));
      realtyTypeCoefficients.add(new RealtyTypeCoefficient(RealtyType.HOUSE, 1.5));
      realtyTypeCoefficients.add(new RealtyTypeCoefficient(RealtyType.ROOM, 1.3));
      List<RealtyTypeCoefficient> all = repository.findAll();
      realtyTypeCoefficients.removeAll(all);
      repository.saveAll(realtyTypeCoefficients);
    };
  }
}
