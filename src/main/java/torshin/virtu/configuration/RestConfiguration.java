package torshin.virtu.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import torshin.virtu.model.Contract;
import torshin.virtu.model.Insurer;

@Configuration
public class RestConfiguration implements RepositoryRestConfigurer {

  /**
   * Override this method to add additional configuration.
   *
   * @param config Main configuration bean.
   */
  @Override
  public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
    config.exposeIdsFor(Contract.class, Insurer.class);
  }
}
