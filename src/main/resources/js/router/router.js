import Vue from 'vue'
import VueRouter from 'vue-router'
import Contracts from 'components/contracts/Contracts.vue'
import ContractForm from 'components/contracts/edit/ContractForm.vue'
import SelectInsurerForm from 'components/insurer/SelectInsurerForm.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/', component: Contracts,
  },
  {
    path: '/edit/:id', component: ContractForm, children: [
      {
      path: 'insurer', component: SelectInsurerForm
    }]
  },
  {path: '*', component: Contracts},

]

export default new VueRouter({
  mode: 'history',
  routes
})