import Vue from 'vue'
import VueResource from 'vue-resource'
import App from 'pages/App.vue'
import router from 'router/router'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';

Vue.use(VueResource)

new Vue({
  el: '#app',
  router,
  render: a => a(App),
})